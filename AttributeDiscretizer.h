/*
Relevant Interval Rules -- a program to extract relevant interval rules from datasets with numeric attributes.
Copyright (C) 2022, Thomas Guyet

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef ATTRIBUTEDISCRETIZER_H_
#define ATTRIBUTEDISCRETIZER_H_

#include <vector>

#include <boost/numeric/ublas/matrix.hpp>
using namespace boost::numeric::ublas;


class AttributeDiscretizer {
protected:
	AttributeDiscretizer(){};
public:
	virtual ~AttributeDiscretizer(){};

	/**
	 * Discretisation par des nombres constants de valeurs entre deux separations, sans prendre en compte les repetitions
	 *
	 * \param sortedvalues tableau de valeurs ordonnées
	 * \param k nombre de séparations, correspond au nombre d'éléments du vecteur retourné (si la taille de sortedvalues le permet)
	 *
	 * \return les modalitées : ce sont les limites de séparation entre groupes (vecteur de taille au plus k)
	 * Le vecteur de modalité commence par la plus petite valeur et se termine par la plus grande valeur.
	 */
	static std::vector<double> discretize(const std::vector<double> &sortedvalues, unsigned int k);

	/**
	 * Discretisation par séparation des valeurs en ensemble de valeurs distincts de même taille. Contrairement à
	 * discretize, les doublons sont pris en charges
	 *
	 * \warning le vecteur en entré est modifiée pour supprimer les doublons : taille et contenu différent. Il reste ordonné !
	 * \param sortedvalues tableau de valeurs ordonnées
	 * \param k nombre de séparations, correspond au nombre d'éléments du vecteur retourné (si le nombre de valeurs distinctes de sortedvalues le permet)
	 *
	 * \return les modalitées : ce sont les limites de séparation entre groupes (vecteur de taille au plus k).
	 * Le vecteur de modalité commence par la plus petite valeur et se termine par la plus grande valeur.
	 *
	 *
	 * Les valeurs des modalités intermédiaires sont obtenues en prenant la milieu entre les valeurs (distinctes)
	 * qui séparent les ensembles. On espère ainsi gagné un peu en généralisation par rapport à prendre uniquement
	 * la valeur dans les données.
	 */
	static std::vector<double> discretize_distinct(std::vector<double> &sortedvalues, unsigned int k);


	/**
	 * Discretisation par des nombres constants de valeurs entre deux separations, sans prendre en compte les repetitions
	 *
	 * \param m matrice de données (dimension 1: les attributs, dimension 2: les objets)
	 * \param attribute id de l'attribut à discretiser
	 * \param k nombre de séparations, correspond au nombre d'éléments du vecteur retourné (si la taille de m le permet)
	 *
	 * \return les modalitées : ce sont les limites de séparation entre groupes
	 */
	static std::vector< std::vector<double> > discretize(const matrix<double> &m, unsigned int k);
};

#endif /* ATTRIBUTEDISCRETIZER_H_ */
