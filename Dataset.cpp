/*
Relevant Interval Rules -- a program to extract relevant interval rules from datasets with numeric attributes.
Copyright (C) 2022, Thomas Guyet

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "Dataset.h"
#include <boost/tokenizer.hpp>
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

//personal function to read double from string
static double read(std::string s, bool *isnumber =NULL);

Dataset::Dataset():_dim(0) {}

Dataset::~Dataset() {}


/**
 * Function to add examples to the class cl from a CSV file (dimensions must agree)
 */
int Dataset::addFile(int cl, string filename) {
	using namespace boost;
	ifstream in(filename.c_str());
	if (!in.is_open()) {
		cerr << "Error while testing file to add "<< filename <<": can not open the file"<< endl;
		return 0;
	}

	int n = std::count(std::istreambuf_iterator<char>(in), std::istreambuf_iterator<char>(), '\n');
	in.seekg(0, ios::beg);
	//n is the number of line in the file

	typedef tokenizer< boost::char_separator<char> > Tokenizer;
	boost::char_separator<char> sep("\t;");

	std::vector< string > vec;
	string line;
	if( getline(in,line) ) {
		//just read the first line to determine the number of dimensions
		Tokenizer tok(line, sep);
		vec.assign(tok.begin(),tok.end());

		unsigned int dim = vec.size()-1;
		if( _dim==0) {
			_dim=dim;
		} else if(dim!=_dim) {
			cerr << "Error while testing file to add "<< filename <<": invalid dimension, expected dimension "<< _dim<<" but get "<< dim << endl;
			return 0;
		}

	} else {
		cerr << "Error while testing file to add "<< filename <<": can not read the file"<< endl;
		return 0;
	}
	in.seekg(0, ios::beg);

	//Prepare the dataset[cl]

	int decay = 0;
	if (_datasets.find(cl) == _datasets.end()) {
		//create a new matrix
		_datasets[cl] = matrix<double>(_dim,n);
		_cl.push_back(cl);
		_clattrs[cl]=class_attributes();
		_clattrs[cl].names = vec[_dim];//name of the class
		_clattrs[cl].nbex = n;
	} else {
		if(_datasets[cl].size1()!=_dim) {
			cerr << "Error: les données doivent être de même dimension" << endl;
			return 0;
		}
		//resize dataset
		decay = _datasets[cl].size2();
		_datasets[cl].resize(_dim, _datasets[cl].size2()+n);
		_clattrs[cl].nbex += n;
	}

	//fill the matrix with new examples
	unsigned i=0, j;

	while (getline(in,line)) {
		j=0; //size1, dimensions
		Tokenizer tok(line, sep);
		vec.assign(tok.begin(),tok.end());

		std::vector< string >::iterator it=vec.begin();
		bool isnumber;
		while(it!=vec.end() && j<_dim) {
			double d=read(*it, &isnumber);
			if( isnumber ) {
				_datasets[cl](j,decay+i)=d;
			}
			it++;
			j++;
		}
		i++;
	}

	//Close file
	in.close();

	return 1;
}

/**
 * Add one example
 * @param cl class of the new example
 * @param d the new example
 * @param name class name (if any)
 */
void Dataset::addData(int cl, const std::vector<double>& d, std::string name) {
	if( _dim==0) { //first data insertion
		_dim = d.size();
	} else if( d.size() != _dim) { //dimension assertion
		cerr << "Dataset::addData: error, dimensions must agree!"<<endl;
		return;
	}
	if( _datasets.find(cl) == _datasets.end()) {
		//create a new matrix from d
		_datasets[cl] = matrix<double>(_dim,1);
		unsigned int dim=0;
		for(double v : d)_datasets[cl](dim++,0)=v;
		_cl.push_back(cl);
		_clattrs[cl]=class_attributes();
		_clattrs[cl].names = name;
		_clattrs[cl].nbex = 1;
	} else {
		matrix<double> &ds = _datasets[cl];
		unsigned int id = ds.size2();
		ds.resize(_dim, ds.size2()+1);
		unsigned int dim=0;
		for(double v : d) {
			ds(dim, id)=v;
			dim++;
		}
		_clattrs[cl].nbex += 1;
	}
}

void Dataset::addData(int cl, matrix<double> d, string name) {
	if( _dim==0) { //first data insertion
		_dim = d.size1();
	} else if( d.size1() != _dim) { //dimension assertion
		cerr << "Dataset::addData: error, dimensions must agree!"<<endl;
		return;
	}

	if (_datasets.find(cl) == _datasets.end()) {
		//create a new matrix from d
		_datasets[cl] = d;
		_cl.push_back(cl);
		_clattrs[cl]=class_attributes();
		_clattrs[cl].names = name;
		_clattrs[cl].nbex = d.size2();
	} else {
		//resize dataset (add new rows)
		int decay = _datasets[cl].size2();
		_datasets[cl].resize(_dim, _datasets[cl].size2()+d.size2());
		//fill the matrix with new examples
		for(unsigned i=0; i < d.size1(); ++i)
			for (unsigned j=0; j < d.size2(); ++j)
				_datasets[cl](i, decay+j) = d(i,j);
		_clattrs[cl].nbex += d.size2();
	}
}


static double read(std::string s, bool *isnumber) {
	double d=0;
	int dec=0;
	if(isnumber) *isnumber=true;

	std::string::iterator it=s.begin();
	while( it!=s.end() ) {
		char c = *it;
		if( c!='.' && c!=',' && !isdigit(c)) {
			if(isnumber) *isnumber=false;
			return 0;
		}
		if( c=='.' || c==',' ) {
			dec=1;
			it++;
			continue;
		}

		int temp = c - '0';
		if( !dec ) {
			d=d*10 + temp;
		} else {
			d = d + ((double)temp)*pow(10,-dec);
			dec++;
		}

		it++;
	}

	return d;
}

