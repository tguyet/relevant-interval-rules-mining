/*
Relevant Interval Rules -- a program to extract relevant interval rules from datasets with numeric attributes.
Copyright (C) 2022, Thomas Guyet

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "ClosedIntervalMiner.h"
#include "Context.h"
#include "AttributeDiscretizer.h"


#include <fstream>
#include <vector>
#include <set>
#include <algorithm>
#include <string>
#include <cassert>
#include <iostream>
#include <functional>
#include <boost/tokenizer.hpp>


unsigned int ClosedIntervalMiner::compute_support(IntervalPattern IP)
{
	unsigned int support=0;
	assert(IP.size() == _data.size1());
	/*
	if( IP.size() !=  _data.size1() ) {
		std::cerr << "error ClosedIntervalMiner::compute_support : incompatible dimensions"<<std::endl;
	}
	*/
	/*
	for(unsigned i=0; i<_data.size2(); i++) {
		bool ok=true;
		for(unsigned j=0; j<_data.size1(); j++) {
			if( !IP[j].support( _data(j,i) ) ) {
				ok=false;
				break;
			}
		}

		if( ok ) {
			support++;
		}
	}*/

	for(matrix<double>::iterator2 i=_data.begin2(); i!=_data.end2(); i++) {
		bool ok=true;
		std::vector<Interval>::const_iterator jj=IP.begin();
		for(matrix<double>::iterator1 j=i.begin(); j!=i.end(); j++, jj++) {
			if( !(*jj).support( *j ) ) {
				ok=false;
				break;
			}
		}

		if( ok ) {
			support++;
		}
	}
	return support;
}

bool ClosedIntervalMiner::is_frequent(IntervalPattern IP, unsigned int threshold) {

	unsigned int support=0;
	/*
	if( IP.size() !=  _data.size1() ) {
		std::cerr << "error ClosedIntervalMiner::compute_support : incompatible dimensions"<<std::endl;
	}*/
	assert(IP.size() == _data.size1());
	/*
	for(unsigned i=0; i<_data.size2(); i++) {
		bool ok=true;
		for(unsigned j=0; j<_data.size1(); j++) {
			if( !IP[j].support( _data(j,i) ) ) {
				ok=false;
				break;
			}
		}

		if( ok ) {
			support++;
			if( support>=threshold ) return true;
		}
	}
	*/

	for(matrix<double>::iterator2 i=_data.begin2(); i!=_data.end2(); i++) {
			bool ok=true;
			std::vector<Interval>::const_iterator jj=IP.begin();
			for(matrix<double>::iterator1 j=i.begin(); j!=i.end(); j++, jj++) {
				if( !(*jj).support( *j ) ) {
					ok=false;
					break;
				}
			}
			if( ok ) {
				support++;
				if( support>=threshold ) return true;
			}
	}
	return false;
}

/**
 * Retourne la modalité juste à gauche de val (la plus grande des inférieures)
 */
double minleftvalue(double val, std::vector<double> &modalities) {
	std::vector<double>::iterator it;
	//it = find(modalities.begin(), modalities.end(), val);
	//it++; //normalement, il n'y a pas de pb de bord !

	it = std::find_if(modalities.begin(), modalities.end(), std::bind2nd(std::greater<double>(),val));
	assert(it!=modalities.end());
	//std::cout << "minleftvalue "<< val <<  " => "<< *it<<std::endl;
	return *it;
}

/**
 * Retourne la modalité juste à droite de val (la plus petite des supérieures)
 */
double maxrightvalue(double val, std::vector<double> &modalities) {
	std::vector<double>::reverse_iterator it;
	//it = find(modalities.begin(), modalities.end(), val);
	//it--; //normalement, il n'y a pas de pb de bord !

	it = std::find_if(modalities.rbegin(), modalities.rend(), std::bind2nd(std::less<double>(),val));
	assert( it!=modalities.rend() );
	//std::cout << "maxrightvalue "<< val <<  " => "<< *it<<std::endl;
	return *it;
}

//mettre une référence pour le contexte
Context ClosedIntervalMiner::minChangeLeft(Context &c, unsigned i)
{
	double val=minleftvalue(c.IP()[i].l(), modalities[i]);
	Context nc(c);
	nc.specialize_left(val, i);
	return nc;
}

Context ClosedIntervalMiner::minChangeRight(Context &c, unsigned i)
{
	double val=maxrightvalue(c.IP()[i].u(), modalities[i]);
	Context nc(c);
	nc.specialize_right(val, i);
	return nc;
}

/**
 * Le contexte c contient G
 * n attribute number that have been changed
 * p =0 means right, =1 means left : implementation d'un ordre lectic
 */
void ClosedIntervalMiner::process(Context &c, unsigned n, bool p, int verbose)
{
	if(verbose>2) std::cout << "process (n="<< n<< ",p="<<p<<")" << std::endl;

	c.compute_support();
	if( c.support()<_minsup || !c.is_canonical(n, modalities, verbose) ) {
		if(verbose>1) std::cout << "\tnot frequent or not canonical" << std::endl;
		return;
	}

	if(verbose>2) std::cout << "\tadd : " << c << std::endl;
	if( verbose>1 && !(_FCIP.size()%10000) )
		std::cout << "\tnb motifs : "<< _FCIP.size()<<std::endl;
	_FCIP.push_back(c);

	//c.support(); //retient le support de c par effet de bord ! : fait juste avant le test de _minsup
	_FCIP.back().freeids(); //permet de récupérer un peu de mémoire ...

	for(unsigned i=n; i<_data.size1(); i++) { //i est un identifiant de l'attribut traité
		if( c.IP()[i].is_singleton() ) {
			continue;
		}
		if( !p || i!=n ) {
			//Reduction droite
			Context nc=minChangeRight(c, i);
			//nc.setParent(&c);
			process(nc, i, 0, verbose);
		}
		//Reduction gauche
		Context nc=minChangeLeft(c, i);
		//nc.setParent(&c);
		process(nc, i, 1, verbose);
	}
}

std::vector< std::vector<double> > ClosedIntervalMiner::getAttributesValues(const matrix<double> &m)
{
	modalities = std::vector< std::vector<double> >(m.size1());

	for(unsigned i = 0; i<m.size1(); ++i) {
		std::set<double> vals;
		for(unsigned j=0; j<m.size2(); ++j) {
			vals.insert( m(i,j) );
		}
		modalities[i] = std::vector<double>(vals.size());
		std::copy(vals.begin(), vals.end(), modalities[i].begin());
		std::sort(modalities[i].begin(), modalities[i].end());
	}
	return modalities;
}

bool ClosedIntervalMiner::setModalities(std::vector< std::vector<double> > m, bool replace) {
	if( m.size() != _data.size1()) {
		std::cerr << "Les modalities proposés doivent avoir une dimension correspondant aux donnees"<<std::endl;
		return false;
	}
	std::vector< std::vector<double> > mods=getAttributesValues(_data);

	for(unsigned i=0; i<_data.size1();i++) {
		if( m[i].front()>mods[i].front() || m[i].back()<mods[i].back() ) {
			std::cerr << "Les modalities proposés doivent avoir des extremites englobant les données"<<std::endl;
			std::cerr << "\tdimension " << i<<std::endl;
			std::cerr << "\t\tmin propose: " << m[i].front() << ", min values: " <<mods[i].front()<<std::endl;
			std::cerr << "\t\tmax propose: " << m[i].back() << ", max values: " <<mods[i].back()<<std::endl;
			if(replace) {
				if( m[i].front()>mods[i].front() ) {
					std::cerr << "\t\tValeur ajoutée aux modalitées (front) : "<< mods[i].front() <<std::endl;
					std::vector<double> modi=m[i];
					m[i]=std::vector<double>(modi.size()+1);
					m[i][0]=mods[i].front();
					for(unsigned j=0; j<modi.size(); j++) mods[i][j+1]=modi[j];
				}
				if( mods[i].back()>m[i].back() ) {
					std::cerr << "\t\tValeur ajoutée aux modalitées (back) : "<< mods[i].back() <<std::endl;
					std::vector<double> modi=m[i];
					m[i]=std::vector<double>(modi.size()+1);
					for(unsigned j=0; j<modi.size(); j++) mods[i][j+1]=modi[j];
					m[i][modi.size()]=mods[i].back();
				}
			} else {
				return false;
			}
		}
	}

	modalities=m;

	//std::cout << "utilise " << modalities[2].size()<<std::endl;
	return true;
};

void ClosedIntervalMiner::viewModalities(std::ostream &os) const
{
	std::vector< std::vector<double> >::const_iterator it=modalities.begin();
	int i=0;
	while(it!=modalities.end()) {
		os << "mods["<<i<<"]: ";
		std::vector<double>::const_iterator jt=(*it).begin();
		while(jt!=(*it).end()) {
			os << *jt << ",";
			jt++;
		}
		os << std::endl;
		i++;
		it++;
	}
}

Context convexhull(const matrix<double> &data, const std::vector< std::vector<double> > &modalites)
{
	std::vector<unsigned> intent(modalites.size());
	IntervalPattern pattern(modalites.size());
	for(unsigned i=0; i<modalites.size(); i++) {
		pattern[i]=Interval(modalites[i].front(), modalites[i].back());
		intent[i]=i;
	}
	std::vector<unsigned> extent( data.size2() );
	for(unsigned i=0; i<data.size2(); i++) extent[i]=i;

	Context context(data, extent, pattern/*, NULL*/);

	return context;
}

void ClosedIntervalMiner::test_functions(unsigned minsup)
{
	int verbose = 3;
	_minsup=minsup;

	if( modalities.size()!=_data.size1() ) {
		if(verbose) std::cout << "modalities have been generated from data" << std::endl;
		//modalities liste de manière ordonnées toutes les valeurs possibles d'un attribut pour chaque attribut
		modalities=getAttributesValues(_data);
	}

	int i=0;
	std::vector< std::vector<double> >::iterator it=modalities.begin();
	while(it!=modalities.end()) {
		std::cout << "attribute "<< i <<" : " << (*it).size() << ", values : ";
		for(unsigned j=0; j<(*it).size();j++) std::cout << (*it)[j] << ",";
		std::cout << std::endl;
		it++;
		i++;
	}

	//Construction du contexte de base
	Context basecontext = convexhull(_data, modalities);

	std::cout << "contexte : " << basecontext << std::endl;
	std::cout <<" iscanonic :" << basecontext.is_canonical(0, modalities, verbose) << std::endl;

	for(unsigned i=0; i<_data.size1(); i++) {
		Context baseRight = minChangeRight(basecontext, i);
		std::cout << "\tcontexte change droite ("<<i<<"): " << baseRight << std::endl;
		int icr=baseRight.is_canonical(i, modalities, verbose);
		std::cout << "\tiscanonic :" << icr << std::endl;
		if( !icr )
			std::cout << "\t\tClosure :" << baseRight.IP()<<std::endl;
		Context baseLeft = minChangeLeft(basecontext, i);
		std::cout << "\tcontexte change gauche ("<<i<<"): " << baseLeft << std::endl;
		int ic=baseLeft.is_canonical(i, modalities, verbose);
		std::cout << "\tiscanonic :" << ic << std::endl;
		if( !ic )
			std::cout << "\t\tClosure :" << baseRight.IP()<<std::endl;
	}
}

void ClosedIntervalMiner::FrequentClosedIntervalPattern(unsigned minsup, int verbose)
{
	_minsup=minsup;

	if( modalities.size()!=_data.size1() ) {
		if(verbose) std::cout << "modalities have not been proposed : we generate them from data" << std::endl;
		//modalities liste de manière ordonnées toutes les valeurs possibles d'un attribut pour chaque attribut
		modalities=getAttributesValues(_data);
	}

	if(verbose>1) {
		int i=0;
		std::vector< std::vector<double> >::iterator it=modalities.begin();
		while(it!=modalities.end()) {
			std::cout << "attribute " << i << " : " << (*it).size() << std::endl;

			if(verbose>2) {
				for(unsigned i=0; i<(*it).size();i++) std::cout << (*it)[i] << ",";
				std::cout << std::endl;
			}
			it++;
			i++;
		}
	}

	//Construction du contexte de base
	Context basecontext = convexhull(_data, modalities);

	//On ferme ce premier contexte ! (qui ne l'est pas nécessairement avec l'ajustemetn des modalités ???)
	basecontext.close(modalities);

	_FCIP.clear();
	process(basecontext, 0, 0, verbose);
}

static double read(std::string s, bool *isnumber =NULL) {
	double d=0;
	int dec=0;
	if(isnumber) *isnumber=true;

	std::string::iterator it=s.begin();
	while( it!=s.end() ) {
		char c = *it;
		if( c!='.' && c!=',' && !isdigit(c)) {
			if(isnumber) *isnumber=false;
			return 0;
		}
		if( c=='.' || c==',' ) {
			dec=1;
			it++;
			continue;
		}

		int temp = c - '0';
		if( !dec ) {
			d=d*10 + temp;
		} else {
			d = d + ((double)temp)*pow(10,-dec);
			dec++;
		}

		it++;
	}

	return d;
}

/**
 * add data to the
 * @param d is a matrix containing data (size2() is the number of examples, and size1() is the examples dimesion)
 */
void ClosedIntervalMiner::addData(const matrix<double> &d)
{
	using namespace std;
	if(_data.size1()==0) {
		//copy the entire matrix
		_data = d;
	} else {
		if( _data.size1 ()!= d.size1 () ) {
			cerr << "Error: les données doivent être de même dimension" << endl;
			return;
		}
		unsigned int decay = _data.size2();
		//Allocate space for new examples (old exemples remains)
		_data.resize(_data.size1(),decay + d.size2());

		//fill the matrix with new examples
		for(unsigned i = 0; i < d.size1 (); ++ i)
			for (unsigned j = 0; j < d.size2 (); ++ j)
				_data(i, decay+j) = d(i,j);
	}
}

void ClosedIntervalMiner::addData(std::string filename, unsigned nbattr, int verbose)
{
	using namespace std;
	using namespace boost;

	unsigned s=0;

	ifstream in(filename.c_str());
	if (!in.is_open()) {
		cerr << "Error while loading file" <<endl;
		return;
	}

	int n = std::count(std::istreambuf_iterator<char>(in), std::istreambuf_iterator<char>(), '\n');
	in.seekg(0, ios::beg);

	if(_data.size1()==0) {
		_data = matrix<double>(nbattr,n);
	} else {
		if(_data.size1()!=nbattr) {
			cerr << "Error: les données doivent être de même dimension" << endl;
			return;
		}
		s =_data.size2();
		_data.resize(nbattr,n+s);
	}

	typedef tokenizer< boost::char_separator<char> > Tokenizer;
	boost::char_separator<char> sep("\t;");

	std::vector< string > vec;
	string line;

	unsigned i=0, j;

	while (getline(in,line)) {
		j=0;
		Tokenizer tok(line, sep);
		vec.assign(tok.begin(),tok.end());

		//Pour verification des separateurs :
		std::vector< string >::iterator it=vec.begin();
		bool isnumber;
		while(it!=vec.end() && j<nbattr) {
			double d=read(*it, &isnumber);
			if( isnumber ) {
				_data(j,s+i)=d;
			}
			it++;
			j++;
		}
		i++;
	}

	in.close();

	if(verbose) std::cout << _data.size1() << "," << _data.size2() << std::endl;
}
