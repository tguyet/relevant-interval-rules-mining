/*
Relevant Interval Rules -- a program to extract relevant interval rules from datasets with numeric attributes.
Copyright (C) 2022, Thomas Guyet

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CONTEXT_H_
#define CONTEXT_H_

#include <list>
#include <vector>

#include <boost/numeric/ublas/matrix.hpp>
using namespace boost::numeric::ublas;

#include "Interval.h"

/**
 * TODO : retenir des hash-tables de modalités (1 par attribut) contenant le
 * nombre des modalites de sorte à calculer les fermetures
 * transitives très facilement !
 * -> usage mémoire importante mais gain notable d'efficacité
 */
class Context {
	const matrix<double> &_data; //reference aux donnees : dimension1: les attributs, dimension2: les objets

	const std::vector<int> *_ids = nullptr; //< list of ids associated to each example (_ids size is dimension 2 of _data). It is used to compute the support: if not null, the support is the number of different _ids

	//Context *_parent; //pointeur sur le concept parent, moins spécifique

	/**
	 * HACK réduction taille : mais très calculatoire pour retrouver la liste des instances !

	const Context *parent; //pointeur sur le concept parent
	std::vector<unsigned> _extentsub; // identifiants des objets à supprimer par rapport aux objets du parent

	//*/

	std::vector<unsigned> _extent; //identificants des objects dans les données
	unsigned _support;

	IntervalPattern _IP; // _IP est correspond aux intervalles (convex hull) des objets (connexion de Galois)

public:
	Context(const matrix<double> &data, std::vector<unsigned> &extent, IntervalPattern &ip/*, Context *p*/);
	Context(const Context &c);
	virtual ~Context();

	// retourne vrai si le contexte est canonique, ie qu'il est fermé transitivement
	bool is_canonical(unsigned n, std::vector<std::vector<double> > &modalities, int verbose =0);

	bool close(std::vector<std::vector<double> > &modalities);

	void specialize_left(double val, unsigned i);
	void specialize_right(double val, unsigned i);

	// retourne le support du contexte
	void compute_support(){_support=_extent.size();};
	unsigned support() const {return _support;};

	inline IntervalPattern IP() const {return _IP;};

	void freeids();

	//Context *parent() const {return _parent;};
	//void setParent(Context *p){_parent=p;};

	friend std::ostream& operator<<(std::ostream& out, const Context& );

protected:
	// Calcul de la connexion de Galois pour l'interval I
	std::vector<int> connectInterval();
};

#endif /* CONTEXT_H_ */
