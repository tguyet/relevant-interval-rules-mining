/*
Relevant Interval Rules -- a program to extract relevant interval rules from datasets with numeric attributes.
Copyright (C) 2022, Thomas Guyet

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "Interval.h"


/**
 * Operateur d'inclusion stricte
 */
bool operator<(Interval const &I1, Interval const &I2)
{
	return I1<I2 && !( I1==I2 );
}

bool operator<=(Interval const &I1, Interval const &I2)
{
	return I1.u()<=I2.u() && I1.l()>=I2.l();
}

bool operator==(Interval const &I1, Interval const &I2)
{
	return I1.u()==I2.u() && I1.l()==I2.l();
}

bool operator!=(Interval const &I1, Interval const &I2)
{
	return !(I1==I2);
}

std::ostream& operator<<(std::ostream& out, const Interval& I) // output
{
    out << "[" << I.l() << "," << I.u() << "]";
    return out;
}

/**
 * Opérateur d'inclusion stricte !
 * Retourne faux pour des IntervalPatterns identiques !
 */
bool operator<(IntervalPattern const &IP1, IntervalPattern const &IP2)
{
	bool isequal=true;
	IntervalPattern::const_iterator it1=IP1.begin(), it2=IP2.begin();
	while( it1!=IP1.end() && it2!=IP2.end() ) {
		if( !( (*it1)<=(*it2) ) ) {
			// Comparaison des intervalles unidimensionnels
			// si il n'y a pas du tout d'inclusion (non-strict), on arrête
			return false;
		} else if ( (*it1)!=(*it2) ) {
			isequal=false;
		}
		it1++;
		it2++;
	}
	return !isequal;
}

bool operator==(IntervalPattern const &IP1, IntervalPattern const &IP2)
{
	IntervalPattern::const_iterator it1=IP1.begin(), it2=IP2.begin();
	while( it1!=IP1.end() && it2!=IP2.end() ) {
		if( (*it1)!=(*it2) ) {
			return false;
		}
		it1++;
		it2++;
	}
	return true;
}

bool operator!=(IntervalPattern const &IP1, IntervalPattern const &IP2)
{
	return !(IP1==IP2);
}

std::ostream& operator<<(std::ostream& out, const IntervalPattern& IP)
{
	IntervalPattern::const_iterator it=IP.begin();
	while( it != IP.end() ) {
		out << *it << ", ";
		it++;
	}
    return out;
}
