/*
Relevant Interval Rules -- a program to extract relevant interval rules from datasets with numeric attributes.
Copyright (C) 2022, Thomas Guyet

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "ClosedIntervalMiner.h"
#include "RulesMiner.h"
#include "config.h"

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <fstream>
#include <sstream>
#include <set>
#include <boost/numeric/ublas/matrix.hpp>
using namespace boost::numeric::ublas;

void usage(const char *comm);

#define VERSION "1.0"


/**
 *
 */
int main(int argc, char **argv) {
	RulesMiner rminer;
	int support=10, maxfp =100, nbmod=10;
	std::string output_basename;


	if(argc<=1) {
		cerr << "An input file name is required\n";
		usage( argv[0] );
		return EXIT_FAILURE;
	}

	int cl=0;
	RulesMiner::modalities_rules mod_rules = RulesMiner::MODALITIES_NONE;


#if PROFILE
	fprof.open("prof.txt",std::fstream::out | std::fstream::app);
#endif


	for(int i=1;i<argc;i++) {
		if( !strcmp(argv[i],"-h") ) {
			usage(argv[0]);
			return EXIT_SUCCESS;
		} else if(!strcmp(argv[i],"-f") || !strcmp(argv[i],"--fmin")) {
			i++;
			if( i<0 || 1!=sscanf(argv[i],"%d",&support) || support<0 ) {
				cerr << "Parameter error: Expected positive integer after --fmin (-f)\n";
				usage( argv[0] );
				exit( EXIT_FAILURE );
			}
		} else if(!strcmp(argv[i],"-mfp") || !strcmp(argv[i],"--maxfp")) {
			i++;
			if( 1!=sscanf(argv[i],"%d",&maxfp) || maxfp<0 ) {
				cerr << "Parameter error: Expected positive integer after --maxfp (-mfp)\n";
				usage( argv[0] );
				exit( EXIT_FAILURE );
			}
		} else if(!strcmp(argv[i],"-eqmod")) {
			mod_rules = RulesMiner::EQUIREP_MODALITIES;
			i++;
			if( 1!=sscanf(argv[i],"%d",&nbmod) || nbmod<0 ) {
				cerr << "Parameter error: Expected positive integer after --eqmod\n";
				usage( argv[0] );
				exit( EXIT_FAILURE );
			}
		} else if(!strcmp(argv[i],"-rule mod")) {
			mod_rules = RulesMiner::RULES_MODALITIES;
			i++;
			rminer.addRules(argv[i], 9);
		} else if(!strcmp(argv[i],"-o")) {
			i++;
			output_basename=argv[i];
		} else {
			if( !rminer.addFile(cl, argv[i]) ) {
				cerr << "File error: invalid example file\n";
				usage( argv[0] );
				exit( EXIT_FAILURE );
			}
			cl++;
		}
	}

	if( cl<2 ) {
		cerr << "Parameter error: Expected at least two example files\n";
		usage( argv[0] );
		exit( EXIT_FAILURE );
	}

#if PROFILE
	fprof << support << " " << maxfp << " " << mod_rules << " " << nbmod;
#endif

	cout << rminer.dataset().nbex(0) << "+ "  << rminer.dataset().nbex(1) << "+ " << rminer.dataset().nbex(2) << " = " << rminer.nbex() << "." <<endl;

	rminer.setMinSupport( (support*rminer.nbex())/100 );
	rminer.setMaxFP( (maxfp*rminer.nbex())/100 );
	rminer.setDefaultModsNb(nbmod);

	//TODO : tester une éventuelle fuite avec Valgrind ...
	// 	-> 31/05 rien de notable

#if NDEBUG
	rminer.mine(mod_rules,0);
#else
	rminer.mine(mod_rules,1);
#endif

	if( !output_basename.empty() ){
		std::ofstream outputfile;
		stringstream ss;
		ss << output_basename;
		ss << ".txt";
		outputfile.open(ss.str().c_str());
		rminer.printrules(outputfile);
		outputfile.close();
	} else {
		rminer.printrules();
	}

#if PROFILE
	fprof << endl;
	fprof.close();
#endif

	return EXIT_SUCCESS;
}


void usage(const char *comm)
{
	printf("*******************\n");
	printf("Interval Pattern Miner program, v.%s\n\n", VERSION);
	printf("Author: Guyet Thomas, Inria\n");
	printf("Year: 2022\n");
	printf("*******************\n");
	printf("*******************\n\n");
	printf("command : %s [-f %%d] [-mfp %%d] [-eqmod %%d] [-o basename] datafile_cl0 datafile_cl1 ...\n\n", comm);
	printf("Options :\n");
	printf("\t --fmin (-f) : support threshold (in percent)\n");
	printf("\t --maxfp (-mfp) : maximum false positive (in percent)\n");
	printf("\t -eqmod : enable modalities reduction with equi-probabilities, and defines the maximum number of modalities per dimension (default 10)\n");
	printf("\t -o : basename to output results (stddev by default)\n");
	printf("\t  datafile_cl0 ... : list of files, with one file per class (at least two files), each file is a csv file (sep=\t or ;) with quantitative attribute. Last colonne is the class label.");
	printf("*******************\n");
}
