/*
Relevant Interval Rules -- a program to extract relevant interval rules from datasets with numeric attributes.
Copyright (C) 2022, Thomas Guyet

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef RULE_H_
#define RULE_H_

#include "Interval.h"

using namespace std;

/**
 * \brief une règle est une prémise donnée comme un ensemble d'intervalles (type attributs-valeurs) associé à une classe
 *
 */
class IRule {
	IntervalPattern _IP; 	//!< Prémise de la règle
	int _cl; 				//!< classe associée à la règle
	unsigned int _FP, _TP; 	//!< attributs pour les vrai positifs et les faux positifs (en nombre !)
public:
	IRule():_cl(-1),_FP(0), _TP(0){};
	IRule(IntervalPattern ip, int cl):_IP(ip), _cl(cl),_FP(0), _TP(0){};
	virtual ~IRule(){};

	void setClass(int val){_cl=val;};
	void setPremise(IntervalPattern ip){_IP=ip;};

	IntervalPattern IP() const {return _IP;};
	int cl() const {return _cl;};
	unsigned int FP() const {return _FP;};
	unsigned int TP() const {return _TP;};
	void setFP(unsigned int val) {_FP=val;};
	void setTP(unsigned int val) {_TP=val;};

	friend std::ostream& operator<<(std::ostream& out, const IRule& I);
};

#endif /* RULE_H_ */
