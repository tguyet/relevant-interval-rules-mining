/*
Relevant Interval Rules -- a program to extract relevant interval rules from datasets with numeric attributes.
Copyright (C) 2022, Thomas Guyet

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


#include "RulesMiner.h"
#include "ClosedIntervalMiner.h"
#include "AttributeDiscretizer.h"
#include "config.h"

#include <fstream>
#include <boost/tokenizer.hpp>
#include <cmath>
#include <set>
#include <algorithm>
#include <functional>
#include <boost/tokenizer.hpp>
#include <chrono>
#include <ctime>

using namespace std;

/**
 * Extract rules for all classes
 */
void RulesMiner::mine(modalities_rules use_modalities, int verbose)
{
	_extractedrules.clear();
	if(verbose) {
		cout << "=================== INTERVAL RULE MINING ==================="<<std::endl;
		cout << "\t\tfmin="<< _minsupport<<std::endl;
		cout << "\t\tfmaxfp="<< _maxfp<<std::endl;
		cout << "============================================================" << std::endl;
	}

	//extraire les règles uniquement pour la prédiction de chaque classe
	for(int i : _datasets.classes()) {
		mine(i, _minsupport, _maxfp, use_modalities, verbose);
	}
}

/**
 * Extract rules for the single class cl (as positive)
 */
void RulesMiner::mine_class(int cl, modalities_rules use_modalities, int verbose) {

	_extractedrules.clear();
	if(verbose) {
		cout << "=================== INTERVAL RULE MINING ==================="<<std::endl;
		cout << "\t\tfmin="<< _minsupport<<std::endl;
		cout << "\t\tfmaxfp="<< _maxfp<<std::endl;
		cout << "\t\tcl="<< cl<<std::endl;
		cout << "\t\tdim="<< dim()<<std::endl;
		cout << "\t\t#pos="<< _datasets[cl].size2() <<std::endl;
		int neg=0;
		for(int c : _datasets.classes()){
			if(c==cl) continue;
			neg += _datasets[c].size2();
		}
		cout << "\t\t#neg="<< neg <<std::endl;
		cout << "============================================================" << std::endl;
	}

	//extraire les règles uniquement pour la prédiction de cl
	mine(cl, _minsupport, _maxfp, use_modalities, verbose);
}

/**
 * \brief Fonction pour l'extraction des règles de prédiction de la classe cl. Les autres
 * classes sont considérées comme des exemples négatifs
 * \param cl classe d'intérêt
 * \param support support minimal que doit avoir une règle
 * \param maxfp nombre maximal de négatifs
 * \param verbose niveau d'affichage de trace 0: aucune trace, 1: traces de déroulement, 2: traces de résultats intermédiaires
 */
void RulesMiner::mine(int cl, unsigned support, unsigned maxfp, RulesMiner::modalities_rules use_modalities, int verbose)
{
	if(verbose) {
		cout << "******* Règles pour la classe " << cl << " *******" << std::endl;
	}

#if PROFILE
	fprof << " | " << cl << " ";
	std::chrono::time_point<std::chrono::high_resolution_clock> start, end;
	start = std::chrono::system_clock::now();
#endif

	//ETAPE 1 : Simplification du jeu d'exemples
	ClosedIntervalMiner miner;
	miner.addData( _datasets[cl] );
	miner.getAttributesValues( miner.data());

	switch(use_modalities) {
	case RulesMiner::RULES_MODALITIES:
		miner.setModalities( getModalities() );
		break;
	case RulesMiner::EQUIREP_MODALITIES:
		miner.setModalities( AttributeDiscretizer::discretize(miner.data(), default_mobsnb) );
		break;
	default:
		break;
	}
	if(verbose) {
		std::cout << "********* Modalities *********\n"<< std::endl;
		miner.viewModalities();
	}

#if PROFILE
	fprof << miner.nbModalities(cl) << " ";
#endif


	//ETAPE 1 : Extraction des motifs clos fréquents

	miner.FrequentClosedIntervalPattern(support, verbose);
	list<Context> PFCIP=miner.FCIP(); //Ensemble des positifs

#if PROFILE
	fprof << PFCIP.size() << " ";
#endif

	if(verbose) std::cout<< "# clos : "<<PFCIP.size()<<endl;
	//La liste est ordonnée par les plus spécifiques d'abord !
	if(verbose>1) {
		std::cout << "********* FCIPs *********\n"<< std::endl;
		std::list<Context>::iterator itc=PFCIP.begin();
		while( itc!=PFCIP.end() ) {
			std::cout << *itc << std::endl;
			itc++;
		}
		std::cout << "*************************\n"<< std::endl;
		std::cout << "TOTAL # : "<< PFCIP.size() << std::endl;
		std::cout << "*************************\n"<< std::endl;
	}

	//Calcul des couvertures pour les négatifs
	ClosedIntervalMiner miner_neg;
	for( int negcl : _datasets.classes() ) {
		if( negcl==cl ) {
			continue;
		}
		miner_neg.addData( _datasets[negcl] );
	}

	//ETAPE 2 : on construit l'ensemble des règles en élaguant les
	// règles qui ont des FP trop importants

	std::list<IRule> lrules;
	std::list<Context> FCIP=miner.FCIP();
	std::list<Context>::iterator it=FCIP.begin();
	while( it!=FCIP.end() ) {
		unsigned int FP=miner_neg.compute_support( (*it).IP() );
		if( FP>maxfp ) {
			it=FCIP.erase(it);
		} else {
			IRule r((*it).IP(), cl);
			r.setFP( FP );
			r.setTP( (*it).support() );

			//cout << r <<endl;
			lrules.push_back(r);
			//cout << lrules.back() <<endl;
			it++;
		}
	}
	//TODO HACK vider la mémoire de miner
	FCIP.clear();


#if PROFILE
	fprof << lrules.size() << " ";

	end = std::chrono::system_clock::now();
	int elapsed_seconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();

	fprof << elapsed_seconds << " ";
#endif

	if(verbose) std::cout<< "# rules avant simplification : "<<lrules.size()<<endl;

	//ETAPE 3 : on supprime les règles NON-RELEVANTE (au sens de Garriga),
	// ie qui ont une version moins spécifique avec le même FP
	simplify(lrules, verbose);

#if PROFILE
	fprof << lrules.size() << " ";

	end = std::chrono::system_clock::now();
	elapsed_seconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();

	fprof << elapsed_seconds << " ";
#endif
	if(verbose) std::cout<< "# rules après simplification : "<<lrules.size()<<endl;

	if(verbose>1) {
		std::list<IRule>::iterator itr=lrules.begin();
		while(itr!=lrules.end()) {
			cout << *itr << endl;
			itr++;
		}
	}

	std::copy(lrules.begin(), lrules.end(), std::back_inserter(_extractedrules) );
	if(verbose) std::cout<< "# total rules : "<<_extractedrules.size()<<endl;
}


/**
 * Fonction pour la suppression des règles qui ont une version
 * plus générique (moins spécifique) avec le même FP
 *
 * On suppose que la liste des règles est ordonnée de sorte qu'il
 * n'existe pas de règle plus générique à une règle i avant pas la position i
 *
 * Version brutale !
 */
void RulesMiner::simplify(std::list<IRule>& rules, int verbose)
{
	list<IRule>::iterator itr=rules.begin();
	while(itr!=rules.end()) {
		list<IRule>::iterator itr2=itr;
		itr2++;
		while(itr2!=rules.end()) {
			if( (*itr2).FP()==(*itr).FP() && (*itr).IP()<(*itr2).IP() ) {
				if(verbose>1) cout << "NOT RELEVANT : "<< *itr2 <<endl;
				itr2=rules.erase(itr2);
			} else {
				itr2++;
			}
		}
		itr++;
	}
}

/**
 * Construit un ensemble de modalités à partir de règles
 */
std::vector< std::vector<double> > RulesMiner::getModalities()
{
	std::vector< std::vector<double> > mods(dim());

	for(unsigned i=0; i< dim(); i++) {
		set<double> values; //Ensemble des valeurs pour la dimension i
		list<IRule>::const_iterator it= _rules.begin();
		while(it!=_rules.end()) {
			Interval I=(*it).IP()[i];
			values.insert(I.l());
			values.insert(I.u());
			it++;
		}

		//Recopie des valeurs (sans redondance) dans un vecteur
		mods[i]=std::vector<double>(values.size());
		std::copy(values.begin(), values.end(), mods[i].begin());
		std::sort(mods[i].begin(), mods[i].end());
		//cout <<  mods[i].size() <<endl;
	}
	return mods;
}

static double read(std::string s, bool *isnumber =NULL) {
	double d=0;
	int dec=0;
	if(isnumber) *isnumber=true;

	std::string::iterator it=s.begin();
	while( it!=s.end() ) {
		char c = *it;
		if( c!='.' && c!=',' && !isdigit(c)) {
			if(isnumber) *isnumber=false;
			return 0;
		}
		if( c=='.' || c==',' ) {
			dec=1;
			it++;
			continue;
		}

		int temp = c - '0';
		if( !dec ) {
			d=d*10 + temp;
		} else {
			d = d + ((double)temp)*pow(10,-dec);
			dec++;
		}

		it++;
	}

	return d;
}

/**
 * HACK adding rules do not initialize the dimension for examples: may generate errors !!
 */
int RulesMiner::addRules(string filename, unsigned dim)
{
	ifstream in(filename.c_str());
	if (!in.is_open()) {
		cerr << "Error while loading file" <<endl;
		_rules.clear();
		dim=0;
		return 1;
	}

	typedef boost::tokenizer< boost::char_separator<char> > Tokenizer;
	boost::char_separator<char> sep("\t;");

	std::vector< string > vec;
	string line;

	unsigned i=0, j;
	double d=0.0;

	while( getline(in,line) ) {
		j=0;
		Tokenizer tok(line, sep);
		vec.assign(tok.begin(),tok.end());

		IntervalPattern ip(dim);
		Interval I;

		//Pour verification des separateurs :
		//copy(vec.begin(), vec.end(), ostream_iterator<string>(cout, "|"));
		//cout << "\n----------------------" << endl;
		std::vector< string >::iterator it=vec.begin();
		bool isnumber;
		while(it!=vec.end() && j<2*dim) {
			d=read( *it, &isnumber);
			if( !isnumber ) {
				_rules.clear();
				dim=0;
				return 2;
			}
			it++;
			j++;


			if(j%2) {
				I = Interval(d);
			} else {
				I.setU(d);
				ip[ j/2-1 ]=I;
			}
		}

		if( j==2*dim && it!=vec.end()) {
			//lecture de la classe
			d=read( *it, &isnumber);
			if( !isnumber ) {
				_rules.clear();
				dim=0;
				return 2;
			}
		} else {
			_rules.clear();
			dim=0;
			return 3;
		}

		int cl = std::floor(d);
		IRule r=IRule(ip,cl);
		_rules.push_back( r );

		i++;
	}
	return 0;
}


int RulesMiner::addFile(int cl, string filename) {
	return _datasets.addFile(cl,filename);
}

void RulesMiner::printrules(std::ostream &os) const {
	list<IRule>::const_iterator it= _extractedrules.begin();
	while(it!=_extractedrules.end()) {
		int cl=(*it).cl();
		os << (*it).IP() << "=> " << _datasets.name(cl) << " (TP:" << (*it).TP()<<",FP:"<<(*it).FP()<<")" << std::endl;
		it++;
	}
}
