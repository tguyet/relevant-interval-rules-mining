/*
Relevant Interval Rules -- a program to extract relevant interval rules from datasets with numeric attributes.
Copyright (C) 2022, Thomas Guyet

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CLOSEDINTERVALMINER_H_
#define CLOSEDINTERVALMINER_H_

#include <list>
#include <vector>
#include <string>
#include <iostream>

#include <boost/numeric/ublas/matrix.hpp>
using namespace boost::numeric::ublas;

#include "Interval.h"
#include "Context.h"

class ClosedIntervalMiner {
protected:
	/**
	 * Représentation des données :
	 * - dimension 1 : les attributs
	 * - dimension 2 : les objets
	 */
	matrix<double> _data; 	//Contient les données auxquelles font références les contextes (matrices d'entiers)
	unsigned int _minsup;
	std::list<Context> _FCIP;
	//!> modalities : liste de manière ordonnée toutes les valeurs possibles d'un attribut pour chaque attribut
	std::vector< std::vector<double> > modalities;
public:
	ClosedIntervalMiner():_minsup(1){};
	virtual ~ClosedIntervalMiner(){};

	std::vector< std::vector<double> > getAttributesValues(const matrix<double> &m);
	void FrequentClosedIntervalPattern(unsigned int minsup, int verbose =0);
	void test_functions(unsigned int minsup);

	bool setModalities(std::vector< std::vector<double> > m, bool replace=true);
	unsigned int nbModalities(unsigned int cl) const {if(_data.size1()>cl) return modalities[cl].size(); else return 0;};
	void viewModalities(std::ostream &os = std::cout) const;

	unsigned int compute_support(IntervalPattern IP);
	bool is_frequent(IntervalPattern IP, unsigned int threshold);

	// Chargement d'un fichier de données
	void addData(std::string filename, unsigned int nbattr =9, int verbose =0);

	void addData(const matrix<double> &d);

	const matrix<double> &data(){return _data;};

	std::list<Context> FCIP() const {return _FCIP;};

protected:
	//Implémentation des fonctions de Keytoue :
	Context minChangeRight(Context &c, unsigned int i);
	Context minChangeLeft(Context &c, unsigned int i);
	void process(Context &c, unsigned int n, bool p, int verbose =0);
};

#endif /* CLOSEDINTERVALMINER_H_ */
