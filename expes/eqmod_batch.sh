
#Formatage du nom de la sortie avec des éléments de date et d'heure (unique !)
d=`date '+%d%m_%H%M%S'`
outputfile="ee_"$d".txt" 
outputrfile="rr_"$d".txt"
dataset=haberman


#ecoli
# fonctionne avec fmin=10 eqmod=10
# au maximum 46 valeurs d'attributs

#iris / small_iris

#sacadeau
# jeux de données de Véro

#winequality_red/
#winequality_white/
#P. Cortez, A. Cerdeira, F. Almeida, T. Matos and J. Reis. Modeling wine preferences by data mining from physicochemical properties. In Decision Support Systems, Elsevier, 47(4):547-553, 2009.

#reinitialisation du fichier avec les entêtes des colonnes
echo "supmin maxfp time MaxRSS" > $outputfile
echo "nbrules"> $outputrfile

rm prof.txt
echo "minsup maxfp rulemod eqmod [| clid #mod #posconcepts #rules time #relrules time]" >> prof.txt

for fmin in 20 #5 10 15 20 25 30 35 40 45 50
do
	for maxfp in 10 #5 10 15 20 25 30 35 40 45 50
	do
		for eqmod in 42 44 46 48 #60 70 80 90 100
		do
				echo process $fmin $maxfp $eqmod
				# l'option -n évite le saut de ligne en fin (plus facile a gérer ensuite dans une feuille excel !
				echo -n $fmin $maxfp >> $outputfile
				/usr/bin/time -a -o $outputfile -f " %U %M" ./Release/IntervalRules -f $fmin -mfp $maxfp -eqmod $eqmod -o $dataset `ls ./data/ex_$dataset/*.csv`
				#lines=`wc -l $dataset.txt | cut -f1 -d' '`
				#echo $lines >> $outputrfile
		done
	done
done
