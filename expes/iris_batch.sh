
#Formatage du nom de la sortie avec des éléments de date et d'heure (unique !)
d=`date '+%d%m_%H%M%S'`
dataset=sacadeau
outputfile=$dataset"_ee_"$d".txt" 
rfile=$dataset"_rule_neq"


#ecoli
# fonctionne avec fmin=10 eqmod=10
# au maximum 46 valeurs d'attributs

# iris / small_iris

#sacadeau
# jeux de données de Véro

#winequality_red/
#winequality_white/
#P. Cortez, A. Cerdeira, F. Almeida, T. Matos and J. Reis. Modeling wine preferences by data mining from physicochemical properties. In Decision Support Systems, Elsevier, 47(4):547-553, 2009.

#reinitialisation du fichier avec les entêtes des colonnes
echo "supmin maxfp time MaxRSS" > $outputfile
echo "nbrules"> $rfile".txt"

rm prof.txt
echo "minsup maxfp rulemod eqmod [| clid #mod #posconcepts #rules #relrules time]" >> prof.txt

for fmin in 15 #5 10 15 20 25 30 35 40 45 50
do
	for maxfp in 15 #5 10 15 20 25 30 35 40 45 50
	do
		for eqmod in 10 #10 20 30 40 50 60 70 80 90 100
		do
				echo process $fmin $maxfp $eqmod
				# l'option -n évite le saut de ligne en fin (plus facile a gérer ensuite dans une feuille excel !
				echo -n $fmin $maxfp >> $outputfile
				#echo RUN: ./Release/IntervalRules -f $fmin -mfp $maxfp -eqmod $eqmod -o $rfile `ls ./data/ex_$dataset/*.csv`
				/usr/bin/time -a -o $outputfile -f " %U %M" ./Release/IntervalRules -f $fmin -mfp $maxfp -eqmod $eqmod -o $rfile `ls ./data/ex_$dataset/*.csv`
				#/usr/bin/time -a -o $outputfile -f " %U %M" ./Release/IntervalRules -f $fmin -mfp $maxfp -o $rfile `ls ./data/ex_$dataset/*.csv`
				#lines=`wc -l $dataset.txt | cut -f1 -d' '`
				#echo $lines >> $outputrfile
		done
	done
done
