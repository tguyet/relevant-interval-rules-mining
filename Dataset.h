/*
Relevant Interval Rules -- a program to extract relevant interval rules from datasets with numeric attributes.
Copyright (C) 2022, Thomas Guyet

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DATASET_H_
#define DATASET_H_

#include <map>
#include <vector>
#include <list>
#include <string>
#include <boost/numeric/ublas/matrix.hpp>

using namespace boost::numeric::ublas;


/**
 * Class implementing a dataset for rule learning
 */
class Dataset {
protected:
	unsigned int _dim; //example dimensions

	class class_attributes {
	public:
		std::string names="";
		unsigned int nbex=0;
	};

	std::map<int, matrix<double> > _datasets; //< dataset
	std::map<int, class_attributes> _clattrs; //< names of classes
	std::list<int> _cl;

public:
	Dataset();
	virtual ~Dataset();

	unsigned int dim() const {return _dim;};

	int addFile(int cl, std::string filename);
	void addData(int cl, matrix<double> data, std::string name="");
	void addData(int cl, const std::vector<double>& data, std::string name="");

	/**
	 * Return a reference on the dataset.
	 */
	const matrix<double> &operator[](int cl) const {
		return _datasets.at(cl);
	}

	/**
	 * Return the list of classes
	 */
	const std::list<int> &classes() const {
		return _cl;
	}

	/**
	 * Return the name of the class cl
	 * @param id of the class
	 *
	 * The function catch class id errors
	 */
	std::string name(int cl) const {
		try {
			return _clattrs.at(cl).names;
		} catch (const std::out_of_range& oor) {
			std::cerr << "Out of Range error: " << oor.what() << '\n';
			return "";
		}
	}

	/**
	 * Return the number of examples for classe cl
	 * @param id of the class
	 *
	 * The function catch class id errors
	 */
	unsigned int nbex(int cl) const {
		try {
			return _clattrs.at(cl).nbex;
		} catch (const std::out_of_range& oor) {
			std::cerr << "Out of Range error: " << oor.what() << '\n';
			return 0;
		}
	}

	/**
	 * Return the total number of examples
	 */
	unsigned int nbex() const {
		unsigned int nb=0;
		for( int cl : _cl ) {
			nb += nbex( cl );
		}
		return nb;
	}
};

#endif /* DATASET_H_ */
