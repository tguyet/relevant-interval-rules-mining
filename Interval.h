/*
Relevant Interval Rules -- a program to extract relevant interval rules from datasets with numeric attributes.
Copyright (C) 2022, Thomas Guyet

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef INTERVAL_H_
#define INTERVAL_H_

#include <vector>
#include <iostream>

/**
 * \brief classe de représentation d'un interval
 *
 * Le maximum a été mis en inline pour des raisons de perf !
 */
class Interval {
protected:
	double _l,_u;
public:
	Interval():_l(0),_u(0){};
	Interval(double l, double w):_l(l), _u(w) {};
	Interval(double l):_l(l), _u(l) {};
	virtual ~Interval(){};

	inline double u() const {return _u;};
	inline double l() const {return _l;};

	bool is_singleton() const {return _u==_l;};

	bool support(double v) const { return v<=_u && v>=_l; };

	inline void extend(double v)
	{
		if(v>_u) _u=v;
		if(v<_l) _l=v;
	};
	inline void extendleft(double v){if(v<_l) _l=v;};
	inline void extendright(double v){if(v>_u) _u=v;};

	void setL(double val){_l=val;};
	void setU(double val){_u=val;};

	friend std::ostream& operator<<(std::ostream& out, const Interval& I);
};

/**
 * \brief Classe pour représenter un ensemble de valeurs 'intervalles" pris par
 * des attributs
 */
class  IntervalPattern : public std::vector<Interval>
{
public:
	IntervalPattern():std::vector<Interval>(){};
	IntervalPattern(const IntervalPattern &IP):std::vector<Interval>(IP){};
	IntervalPattern(unsigned size):std::vector<Interval>(size){};

	friend std::ostream& operator<<(std::ostream& out, const IntervalPattern& I);
};


/**
 * operateur de verification de l'inclusion des intervalles
 */
bool operator<(Interval const &I1, Interval const &I2);
bool operator==(Interval const &I1, Interval const &I2);
bool operator!=(Interval const &I1, Interval const &I2);
bool operator<=(Interval const &I1, Interval const &I2);

bool operator<(IntervalPattern const &IP1, IntervalPattern const &IP2);
bool operator==(IntervalPattern const &IP1, IntervalPattern const &IP2);
bool operator!=(IntervalPattern const &IP1, IntervalPattern const &IP2);

#endif /* INTERVAL_H_ */
