/*
Relevant Interval Rules -- a program to extract relevant interval rules from datasets with numeric attributes.
Copyright (C) 2022, Thomas Guyet

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "AttributeDiscretizer.h"
#include <vector>
#include <set>
#include <algorithm>

std::vector<double> AttributeDiscretizer::discretize(const std::vector<double> &m, unsigned int k)
{
	if( m.size()<=k ) {
		std::vector<double> modalities( m.size() );
		unsigned int pos=0;
		while( pos < m.size() ) {
			modalities[pos]=m[pos];
			pos++;
		}
		return modalities;
	}
	std::vector<double> modalities( k );


	float nbval=(float)m.size()/(float)(k-1); //nombre de valeurs par classes

	//premier bord
	modalities[0]=m[0];

	unsigned int idcl=1;
	unsigned int pos=1;
	unsigned int nbcl=1;
	while( pos < m.size() && idcl<(k-1) ) {
		nbcl++;
		if( nbcl>nbval ) {
			modalities[idcl]=m[pos];
			nbcl=1;
			idcl++;
		}
		pos++;
	}

	//dernier bord
	modalities[k-1]=m[m.size()-1];

	return modalities;
}

std::vector<double> AttributeDiscretizer::discretize_distinct(std::vector<double> &mcopy, unsigned int k)
{
	std::vector<double>::iterator it;

	//std::sort(mcopy.begin(),mcopy.end()); //< le vecteur est supposé ordonné !
	it=std::unique_copy (mcopy.begin(), mcopy.end(), mcopy.begin());
	mcopy.resize( std::distance(mcopy.begin(),it) );

	if( mcopy.size()<=k ) {
		std::vector<double> modalities( mcopy.size() );
		unsigned int pos=0;
		assert(mcopy.size()>0);
		modalities[pos]=mcopy[pos];
		pos++;
		while( pos < mcopy.size()-1 ) {
			modalities[pos]=(mcopy[pos-1]+mcopy[pos])/2;
			pos++;
		}
		if( mcopy.size()>1 ) {
			modalities[pos]=mcopy[pos];
		}
		return modalities;
	}

	//ICI, il y a plus de valeurs différentes dans les données que de modalitées souhaitées : il faut discretiser
	std::vector<double> modalities( k );
	float nbval=(float)mcopy.size()/(float)(k-1); //< nombre de valeurs par classes

	//premier bord : commence toujours par la valeur min
	modalities[0] = mcopy[0];

	unsigned int idcl=1;
	unsigned int pos=1;
	unsigned int nbcl=1;
	while( pos < mcopy.size() && idcl<(k-1) ) {
		nbcl++;
		if( nbcl>nbval ) {
			modalities[idcl]=(mcopy[pos-1]+mcopy[pos])/2; //on donne la valeur médianne pour plus de généralisation
			nbcl=1;
			idcl++;
		}
		pos++;
	}

	//dernier bord : termine par la valeur max
	modalities[k-1]=mcopy[mcopy.size()-1];

	return modalities;
}

std::vector< std::vector<double> > AttributeDiscretizer::discretize(const matrix<double> &m, unsigned int k) {
	std::vector< std::vector<double> > mods(m.size1());
	std::set<double> values;

	for(unsigned int i=0; i<m.size1(); i++) {
		values.clear();

		//recopies des valeurs pour l'attribut i
		// It's a set
		// -> no values copy
		// -> values are ordered
		for(unsigned int j=0; j<m.size2();j++) {
			values.insert( m(i,j) );
		}

		//Construct a vector of sorted values
		std::vector <double> vvalues(values.size());
		std::copy(values.begin(), values.end(), vvalues.begin());

		mods[i]=discretize_distinct(vvalues, k);
	}
	return mods;
}

