/*
Relevant Interval Rules -- a program to extract relevant interval rules from datasets with numeric attributes.
Copyright (C) 2022, Thomas Guyet

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "Context.h"


Context::Context(const matrix<double> &data, std::vector<unsigned> &extent, IntervalPattern &ip/*, Context *p*/):
	_data(data),
	//_parent(p),
	_extent(extent),
	_support(0),
	_IP(ip)
{}

Context::Context(const Context &c):
	_data(c._data),
	//_parent(c.parent()),
	_extent(c._extent),
	_support(c._support),
	_IP(c._IP)
{}

Context::~Context() {}

void Context::specialize_left(double val, unsigned i)
{
	//Restriction de _IP (Attributs)
	_IP[i]=Interval( val ,_IP[i].u() );

	//Réduction des extensions
	std::vector<unsigned>::iterator it=_extent.begin();
	//std::list<int> deleted; //liste des indices dans data des objets supprimés
	while( it!=_extent.end() ) {
		if( _data(i, *it)<val ) {
			// On supprime les extentions pour lesquels
			// la valeur de la dimension i ne convient
			// plus par rapport à la modification
			// faites
			//deleted.push_back(*it);
			it = _extent.erase(it);
		} else {
			it++;
		}
	}

	// HACK TODO (?)
	//ICI on peut regarder pour la canonicité en regardant les éléments supprimés :
	// 	-> si aucun objet supprimé n'atteint le bord d'un attribut, alors cet
	//	attribut ne posera pas de problème !
}

void Context::specialize_right(double val, unsigned i)
{
	//Restriction de _IP (Attributs)
	_IP[i]=Interval(_IP[i].l(),val);

	//Réduction des extensions
	std::vector<unsigned>::iterator it=_extent.begin();
	//std::list<int> deleted; //liste des indices dans data des objets supprimés
	while(it!=_extent.end()) {
		if( _data(i, *it )>val ) {
			// On supprime les extentions pour lesquels
			// la valeur de la dimension i ne convient
			// plus par rapport à la modification
			// faites
			//deleted.push_back(*it);
			it = _extent.erase(it);
		} else {
			it++;
		}
	}

	// HACK TODO (?)
	//ICI on peut regarder pour la canonicité en regardant les éléments supprimés :
	// 	-> si aucun objet supprimé n'atteint le bord d'un attribut, alors cet
	//	attribut ne posera pas de problème !
}

/**
 * Retourne la modalité la plus proche ou égale de val par valeur inférieure
 * modalities est ordonné
 */
double closest_left_value(double val, std::vector<double> &modalities) {
	std::vector<double>::iterator it;
	it = std::find_if(modalities.begin(), modalities.end(), std::bind2nd(std::greater<double>(),val));
	if(it==modalities.end()) {
		return modalities.back();
	}
	if( it!=modalities.begin() ) {
		it--;
	}
	//std::cout << "closest left : " << val << " => " << *it << std::endl;
	return *it;
}
/**
 * Retourne la modalité la plus proche ou égale de val par valeur inférieure
 * modalities est ordonné
 */
double closest_right_value(double val, std::vector<double> &modalities) {
	std::vector<double>::reverse_iterator it;
	it = std::find_if(modalities.rbegin(), modalities.rend(), std::bind2nd(std::less<double>(),val));
	if(it==modalities.rend()) {
		return modalities.front();
	}
	if( it!=modalities.rbegin() ) {
		it--;
	}
	//std::cout << "closest right : " << val << " => " << *it << std::endl;
	return *it;
}


bool Context::close(std::vector<std::vector<double> > &modalities)
{
	//Creation de l'interval pattern
	IntervalPattern lIP( _data.size1() );

	//Réduction des extensions
	std::vector<unsigned>::const_iterator it=_extent.begin();

	//Creation des intervalles singleton à partir du premier élément
	for(unsigned i=0; i<_data.size1(); i++) {
		lIP[i] = Interval( _data(i, *it) );
	}
	it++;

	//On continue à calculer
	while(it!=_extent.end()) {
		for(unsigned i=0; i<_data.size1(); i++) {
			lIP[i].extend( _data(i, *it ) );
		}
		it++;
	}

	//On calcule l'extension pour les modalitées disponibles
	for(unsigned i=0; i<_data.size1(); i++) {
		lIP[i] = Interval(closest_left_value(lIP[i].l(), modalities[i]), closest_right_value(lIP[i].u(), modalities[i]) );
	}

	_IP=lIP;
	return true;
}


/**
 * Il faut calculer le convexe_hull des objets pour savoir si on
 * ne peut pas réduire une des dimensions !!
 * \param n une dimension de la modification
 * \param
 */
bool Context::is_canonical(unsigned n, std::vector<std::vector<double> > &modalities, int verbose)
{
	//Creation de l'interval pattern
	IntervalPattern lIP( _data.size1() );

	//Réduction des extensions
	std::vector<unsigned>::const_iterator it=_extent.begin();

	//Creation des intervalles singleton à partir du premier élément
	for(unsigned i=0; i<_data.size1(); i++) {
		lIP[i] = Interval( _data(i, *it) );
	}
	it++;

	//On continue à calculer
	while(it!=_extent.end()) {
		for(unsigned i=0; i<_data.size1(); i++) {
			lIP[i].extend( _data(i, *it ) );
		}
		it++;
	}

#ifndef NDEBUG
	if(verbose>2) std::cout << "avant contraction :"<< lIP <<std::endl;
#endif
	//On calcule l'extension pour les modalitées disponibles
	for(unsigned i=0; i<_data.size1(); i++) {
		lIP[i] = Interval(closest_left_value(lIP[i].l(), modalities[i]), closest_right_value(lIP[i].u(), modalities[i]) );
	}
#ifndef NDEBUG
	if(verbose>2) std::cout << "après contraction :" << lIP <<std::endl;
#endif

	//On se limite à l'égalité dans la mesure où l'inclusion doit être obtenue par construction !
	//test d'egalité
	/*
	unsigned attrid=_IP.size()-1; //attribut de la première difference entre le motif et le clos
	int equal=true;
	IntervalPattern::const_reverse_iterator it1=lIP.rbegin(), it2=_IP.rbegin();
	while( it1!=lIP.rend() && it2!=_IP.rend() ) {
		if( (*it1)!=(*it2) ) {
			equal=false;
			break;
		}
		it1++;
		it2++;
		attrid--;
	}*/

	//18/6/2013 : vers l'avant !
	unsigned attrid=0; //attribut de la première difference entre le motif et le clos
	int equal=true;
	IntervalPattern::const_iterator it1=lIP.begin(), it2=_IP.begin();
	while( it1!=lIP.end() && it2!=_IP.end() ) {
		if( (*it1)!=(*it2) ) {
			equal=false;
			break;
		}
		it1++;
		it2++;
		attrid++;
	}

#ifndef NDEBUG
	if(verbose>2) std::cout << _IP <<std::endl;
	if(verbose>2) std::cout << lIP << ", canonical : " << equal << " || " << attrid<<">"<< n <<std::endl;
#endif

	//ICI si !equal, attrid donne l'indice de l'attribut le plus grand pour qui il y a une modification dans la fermeture !

	if( equal ) {
		return true;
	}

	// On retient la fermeture pour ce contexte !
	_IP=lIP;

	//Si attrid>n, alors le modif est bien canonique : il faut poursuivre l'exploration !
	return attrid>n;
}

/*
 * On regarde si il existe un objet qui n'est pas dans _intent et qui supporte -> NON !!!
 *
 */
/*
bool function_inutile() {
	unsigned i; //iterateur de _data
	unsigned j=0; //iterateur de _intent
	for( i=0; i<_data.size2(); i++) {
		if(_extent[j]==i ) {
			j++;
			continue;
		} else {
			//Vérification des intervalles sur l'objet _data[i]
			unsigned l;
			for( l=0; l< _intent.size(); l++ ) {
				if( _IP[l].support(_data( _intent[l] , i) )  ) {
					return false;
				}
			}
		}
	}
	return true;
}
*/


std::vector<int> Context::connectInterval()
{
	std::vector<int> o(_IP.size());
	unsigned i; //iterateur de _data
	unsigned j=0; //iterateur de _intent
	unsigned k=0; //iterateur de o
	for( i=0; i<_data.size2(); i++) {
		if( _extent[j]==i ) {
			o[k]=i;
			k++;
			j++;
		} else {
			//Vérification des intervalles sur l'objet _data[i]
			unsigned l;
			for( l=0; l< _IP.size(); l++ ) {
				if( _IP[l].support(_data( l , i) )  ) {
					o[k]=i;
					k++;
				}
			}
		}
	}
	return o;
}

void Context::freeids()
{
	_extent.clear();
}

std::ostream& operator<<(std::ostream& out, const Context& c )
{
	out << c.IP() << "(" << c.support() << ")";
	return out;
}
