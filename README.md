# Mining Relevant Interval Rules

Relevant interval rules extend the work of Garriga et al. for mining relevant rules to numeric attributes by extracting interval-based pattern rules. We propose an algorithm that extracts such rules from numeric datasets using the interval-pattern approach from Kaytoue et al. This algorithm has been implemented and evaluated on real datasets.

## Build

The code simply requires a C++ compiler (compatible with C++11 norms). 

It is possible to use CMake to compile the code. In this case, you simply have to execute the following commands:

```console
$ mkdir build; cd build
$ cmake -DCMAKE_BUILD_TYPE=Release ..
$ make
```

When compiled with CMake, the executable is created in the ̀ bin` directory.

The ̀ PROFILE` compilation activates the code profiling. Profiling is automatically activated when the build type is debug. The compilation of type Release disable profiling and some other outputs to improve the overall code efficiency.

## Usage example

The typical execution command is as follows:
```console
$ RelevantIntervalRules [-f %d] [-mfp %d] [-eqmod %d] [-o basename] datafile_cl0 datafile_cl1 ...
```

Parameters:

* `--fmin` (`-f`): support threshold in percent (integer in range [1,100])
* `--maxfp` (̀ -mfp`): maximum false positive in percent (integer in range [1,100])
* `-eqmod`: enable modalities reduction with equi-probabilities, and defines the maximum number of modalities per dimension (default 10) (integer in range [1,100])
* `-o basename`: set the name to output results (`stdout` by default)
* `datafile_cl0 ...`: list of files, with one file per class (at least two files), each file is a csv file (sep= `\t` or `;`) with only numeric attribute. Last column is the class label (discrete attribute).


### Exemple usage on iris Dataset

The following command line extracts all the rules that are in at least 15% of the dataset and with at most 20% of false positives

```console
$ ./RelevantIntervalRules -f 15 -mfp 20 -eqmod 10 `ls ../data/ex_iris/*.csv`
```

The output will be
```50+ 50+ 50 = 150.
[4.3,5.8], [2.3,4.4], [1,1.9], [0.1,0.6], => Iris-setosa (TP:50,FP:0)
[4.35,5.8], [2.3,4.4], [1,1.9], [0.1,0.6], => Iris-setosa (TP:49,FP:0)
[4.3,5.8], [2.6,4.4], [1,1.9], [0.1,0.6], => Iris-setosa (TP:49,FP:0)
[4.3,5.8], [2.3,4.4], [1.05,1.9], [0.1,0.6], => Iris-setosa (TP:49,FP:0)
[4.9,7], [2,3.4], [3,5.1], [1,1.8], => Iris-versicolor (TP:50,FP:8)
[4.9,7], [2.1,3.4], [3,5.1], [1,1.8], => Iris-versicolor (TP:49,FP:8)
[4.9,7.9], [2.2,3.8], [4.5,6.9], [1.4,2.5], => Iris-virginica (TP:50,FP:18)
[4.9,7.9], [2.35,3.8], [4.5,6.9], [1.4,2.5], => Iris-virginica (TP:49,FP:17)
[4.9,7.9], [2.2,3.8], [4.5,6.9], [1.45,2.5], => Iris-virginica (TP:49,FP:14)
```

The first line indicates the number of example read for each class. Then, each line is one of the rules extracted:

* the premise is a list of intervals that gives the interval in which the attributes must lie in (in their order in the dataset file),
* the conclusion is one of the class names,
* the parenthesis indicates the number of true positives and of false positives.

## Licence

Copyright (C) 2022, Thomas Guyet

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

## Need more information about the method?

### References

* Gemma C. Garriga, Petra Kralj, and Nada Lavrac. Closed sets for labeled data. Journal of Machine Learning Research, 9:559–580, 2008
* Mehdi Kaytoue, Sergei O. Kuznetsov, and Amedeo Napoli. Revisiting numerical pattern mining with formal concept analysis. In Proceedings of International Join Conference on Artificial Intelligence (IJCAI), pages 1342–1347, 2011.

### How to cite

* Thomas Guyet, René Quiniou, Véronique Masson. [Mining relevant interval rules](https://hal.inria.fr/hal-01584981). International Conference on Formal Concept Analysis, Jun 2017, Rennes, France. 

```bibtex
@inproceedings{Guyet_ICFCA2017,
  TITLE = {{Mining relevant interval rules}},
  AUTHOR = {Guyet, Thomas and Quiniou, Ren{\'e} and Masson, V{\'e}ronique},
  BOOKTITLE = {International Conference on Formal Concept Analysis},
  SERIES = {Supplementary proceedings of International Conference on Formal Concept Analysis ({ICFCA})},
  YEAR = {2017},
  URL = {https://hal.inria.fr/hal-01584981},
}
```
