/*
Relevant Interval Rules -- a program to extract relevant interval rules from datasets with numeric attributes.
Copyright (C) 2022, Thomas Guyet

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef RULESMINER_H_
#define RULESMINER_H_

#include "Interval.h"
#include "Rule.h"
#include "Context.h"
#include "Dataset.h"

#include <list>
#include <vector>
#include <map>
#include <string>
#include <iostream>

using namespace std;

/**
 * En entrée, les données sont sous la forme d'attributs valeurs (numériques)
 *
 * - Le support minimal (_minsupport) donne le nombre d'exemple qui doivent être au minimum couverte par une règle. C'est le nombre de vrai positifs.
 * - Le nombre maximum de faux positifs (_maxfp) donne le nombre maximum d'instances des négatifs qui peuvent être couverts
 */
class RulesMiner {
protected:
	//unsigned int _dim;				//!< nombre de dimension dans le jeu de données
	//unsigned int _nbex;				//!< nombre total d'exemples chargés
	list<IRule> _rules;			//!<
	std::list<IRule> _extractedrules;	//!<
	std::vector< std::vector<double> > _mods; //!< les modalités désignent les valeurs qui peuvent être prises pour les extremités des règles. Ce peut être l'ensemble des valeurs des données d'entrée ou bien un sous ensemble de ces valeurs.
	std::list<Context> _PFCIP; //positive Frequent Closed Interval Patterns

	unsigned int _minsupport; 		//<! Minimal support
	unsigned int _maxfp;			//<! Maximal false positive (nombre d'instances couvertes)

	unsigned int default_mobsnb;	//<! default number of modalities while discretisation is used !

	//map<int, string> _files; 	//!< liste des fichiers de données : 1 fichier par classe (clé de la map)
	//map<int, string> _clnames;
	Dataset _datasets;

public:
	typedef enum {MODALITIES_NONE, RULES_MODALITIES, EQUIREP_MODALITIES} modalities_rules;

public:
	RulesMiner():_minsupport(200), _maxfp(500), default_mobsnb(10){};
	virtual ~RulesMiner(){};

	void mine(modalities_rules use_modalities =RULES_MODALITIES, int verbose =0);
	void mine_class(int cl, modalities_rules use_modalities =RULES_MODALITIES, int verbose =0);

	int addFile(int cl, string filename);

	int addRules(string filename, unsigned dim);

	/**
	 * This function give a direct access to the dataset (to avoid data copy)
	 */
	Dataset &dataset(){return _datasets;};

	void printrules(std::ostream &os = std::cout) const;

	const std::list<IRule>& rules() const { return _extractedrules;};

	std::vector< std::vector<double> > getModalities();

	unsigned int dim() const {return _datasets.dim();};
	unsigned int nbex() const {return _datasets.nbex();};
	void setMinSupport(unsigned val) {_minsupport=val;};
	void setMaxFP(unsigned val) {_maxfp=val;};
	void setDefaultModsNb(unsigned int nb) {default_mobsnb=nb;};

protected:
	void mine(int cl, unsigned support, unsigned maxfp, modalities_rules use_modalities, int verbose);
	void simplify(std::list<IRule>& rules, int verbose);
};

#endif /* RULESMINER_H_ */
